#!/usr/bin/env python3
from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters
import logging
import time
import random
import configparser
import sys


logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

logger = logging.getLogger(__name__)


def start(bot, update):  # when /start is used echo
    update.message.reply_text("Oon muute titelt. RSÄ")


def unknown(bot, update):  # if /command is unknown
    viesti = ['Juu', '??']
    update.message.reply_text(text=viesti[random.randint(0, 1)])


def error(bot, update, error):
    logger.warning("Update '%s' caused error '%s'", update, error)


def getToken():
    config = configparser.ConfigParser()
    config.read("config")
    return config['DEFAULT']['bot_token']


def yotalo(bot, update):
    today = time.strftime("%w")
    fileName = 'downloadmenu/yotaloruokalista.txt'
    output = ''
    paivat = ['maanantai', 'tiistai', 'keskiviikko',
              'torstai', 'perjantai', 'lauantai', 'EOF']
    if(today == '0'):
        context.bot.send_message(chat_id=update.message.chat_id,
                                 text="Sunnuntaina on paasto.")  # parse_mode='Markdown'
        return
    today = int(today) - 1  # koska  maanantai on 1
    tanaanOn = str("Tänään on {}.".format(paivat[today]))
    output = output + tanaanOn + '\n'  # rakennetaan yksi pitkä str
    try:
        with open(fileName, 'r') as f:
            rivi = f.readline()
            while True:
                rivi = f.readline()
                if(paivat[today] in rivi):  # etsitään tiedostosta rivi jossa on nykyinen päivä
                    break
            while True:
                rivi = f.readline()
                # tarkistetaan onko jo seuraava päivä
                if(paivat[today + 1] in rivi) or (not rivi):
                    break
                elif('Salaattibaari' in rivi) or (not rivi.split()):  # salaattibaari on aina auki
                    pass
                else:
                    output = output + rivi.strip() + '\n'  # rakennetaan yksi pitkä str
            # tulostetaan yksi str(output) parse_mode='Markdown'
            context.bot.send_message(
                chat_id=update.message.chat_id, text=output, parse_mode='Markdown')
    except Exception as ex:
        print("File: {0}. Exception: {1}".format(fileName, ex))
        update.message.reply_text("Hmmm, I don't see any menu. Call 404 now!")


def laseri(bot, update):
    today = time.strftime("%w")
    fileName = 'downloadmenu/laseriruokalista.txt'
    output = ''
    paivat = ['maanantai', 'tiistai', 'keskiviikko',
              'torstai', 'perjantai', 'EOF']
    if(today == '0') or (today == '6'):
        context.bot.send_message(chat_id=update.message.chat_id,
                                 text="Viikonloppuna on paasto.")  # parse_mode='Markdown'
        return
    today = int(today) - 1  # koska  maanantai on 1
    tanaanOn = str("Tänään on {}.".format(paivat[today]))
    output = output + tanaanOn + '\n'  # rakennetaan yksi pitkä str
    try:
        with open(fileName, 'r') as f:
            rivi = f.readline()
            while True:
                rivi = f.readline()
                if(paivat[today] in rivi):  # etsitään tiedostosta rivi jossa on nykyinen päivä
                    break
            while True:
                rivi = f.readline()
                # tarkistetaan onko jo seuraava päivä
                if(paivat[today + 1] in rivi) or (not rivi):
                    break
                elif('Kevyesti' in rivi) or (not rivi.split()):  # kevyesti on aina olemassa
                    pass
                else:
                    output = output + rivi.strip() + '\n'  # rakennetaan yksi pitkä str
            # tulostetaan yksi str(output) parse_mode='Markdown'
            context.bot.send_message(
                chat_id=update.message.chat_id, text=output, parse_mode='Markdown')
    except Exception as ex:
        print("File: {0}. Exception: {1}".format(fileName, ex))
        update.message.reply_text("Hmmm, I don't see any menu. Call 404 now!")


def buffa(bot, update):
    today = time.strftime("%w")
    fileName = 'downloadmenu/lutruokalista.txt'
    output = ''
    paivat = ['Maanantai', 'Tiistai', 'Keskiviikko',
              'Torstai', 'Perjantai', 'EOF']
    if(today == '0') or (today == '6'):
        context.bot.send_message(chat_id=update.message.chat_id,
                                 text="Viikonloppuna on paasto.")  # parse_mode='Markdown'
        return
    today = int(today) - 1  # koska  maanantai on 1
    tanaanOn = str("Tänään on {}.".format(paivat[today].lower()))
    output = output + tanaanOn + '\n'  # rakennetaan yksi pitkä str
    try:
        with open(fileName, 'r') as f:
            rivi = f.readline()
            while True:
                rivi = f.readline()
                if(paivat[today] in rivi):  # etsitään tiedostosta rivi jossa on nykyinen päivä
                    break
            while True:
                rivi = f.readline()
                # tarkistetaan onko jo seuraava päivä
                if(paivat[today + 1] in rivi) or (not rivi):
                    break
                else:
                    output = output + rivi.strip() + '\n'  # rakennetaan yksi pitkä str
            # tulostetaan yksi str(output) parse_mode='Markdown'
            if(today == 4):
                output = output + 'Muista, että buffa menee kiinni klo 16.30!'
            context.bot.send_message(
                chat_id=update.message.chat_id, text=output, parse_mode='Markdown')
    except Exception as ex:
        print("File: {0}. Exception: {1}".format(fileName, ex))
        update.message.reply_text("Hmmm, I don't see any menu. Call 404 now!")


def main():

    updater = Updater(getToken())  # start of the bot

    dp = updater.dispatcher

    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("buffa", buffa))
    dp.add_handler(CommandHandler("laseri", laseri))
    dp.add_handler(CommandHandler("yotalo", yotalo))
    dp.add_handler(MessageHandler(Filters.command, unknown))

    dp.add_error_handler(error)

    updater.start_polling()  # start the bot

    updater.idle()


if __name__ == "__main__":
    main()
