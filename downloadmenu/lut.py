import urllib.request
import bs4

def lut():
    sauce = urllib.request.urlopen(
        'http://www.kampusravintolat.fi/fi/lutbuffet').read()
    soup = bs4.BeautifulSoup(sauce, 'lxml')
    fileName = 'lutruokalista.txt' #when writing a file use UTF-8 encoding
    nums = set('0123456789')
    paivat = ['Maanantai', 'Tiistai', 'Keskiviikko', 'Torstai', 'Perjantai', 'EOF']
    try:
        with open(fileName, 'w') as menuFile:
            for paragraph in soup.find_all('td'):
                # skipping all line with numbers 'Salaatti' or empty lines
                if (any((c in nums) for c in paragraph.text)) or ('Salaatti' in paragraph.text) or (not paragraph.text.strip()):
                    pass
                else:
                    menuFile.write("{}\n".format(paragraph.text.replace('*', '')))
            menuFile.write('EOF')
    except PermissionError:
        print("cant write a file")
